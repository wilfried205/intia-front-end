

import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Clients from './Clients';
import ClientForm from './ClientForm';

function App() {
  return (
    <Router>
      <div>
        <h1>Mon application</h1>
        <nav>
          <ul>
            <li>
              <Link to="/">Accueil</Link>
            </li>
            <li>
              <Link to="/clients">Clients</Link>
            </li>
          </ul>
        </nav>
        <Routes>
          <Route path="/clients">
            <Clients />
          </Route>
          <Route path="/client/:id">
            <ClientForm />
          </Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
