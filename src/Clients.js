import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Clients() {
  const [clients, setClients] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchClients = () => {
    // Simuler une requête API pour obtenir la liste des clients
    setLoading(true);
    setTimeout(() => {
      setClients([
        { id: 1, nom: 'Doe', prenom: 'John', adresse: '123 Rue Principale' },
        { id: 2, nom: 'Smith', prenom: 'Jane', adresse: '456 Rue Secondaire' },
      ]);
      setLoading(false);
    }, 1000);
  };

  const deleteClient = (id) => {
    // Simuler une requête API pour supprimer un client
    setClients(clients.filter((client) => client.id !== id));
  };

  // Appeler fetchClients lors du premier rendu
  useEffect(() => {
    fetchClients();
  }, []);

  if (loading) {
    return <div>Chargement...</div>;
  }

  return (
    <div>
      <h2>Liste des clients</h2>
      <ul>
        {clients.map((client) => (
          <li key={client.id}>
            {client.nom} {client.prenom} - {client.adresse}
            <button onClick={() => deleteClient(client.id)}>Supprimer</button>
            <Link to={`/client/${client.id}`}>Modifier</Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Clients;