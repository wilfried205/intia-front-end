import React, { useState, useEffect } from 'react';
import { useParams, useNavigate  } from 'react-router-dom';

function ClientForm() {
  const { id } = useParams();
  const history = useNavigate ();
  const [client, setClient] = useState({ nom: '', prenom: '', adresse: '' });

  const fetchClient = () => {
    // Simuler une requête API pour obtenir les détails d'un client
    // en fonction de l'ID passé dans les paramètres d'URL
    setClient({ id: 1, nom: 'Doe', prenom: 'John', adresse: '123 Rue Principale' });
  };

  const handleInputChange = (event) => {
    setClient({ ...client, [event.target.name]: event.target.value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Simuler une requête API pour ajouter ou mettre à jour un client
    console.log('Client soumis :', client);
    history.push('/clients');
  };

  useEffect(() => {
    if (id) {
      fetchClient();
    }
  }, [id]);

  return (
    <div>
      <h2>{id ? 'Modifier' : 'Ajouter'}Voici la suite du code pour le composant `ClientForm.js` :

```jsx
      Client</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>
            Nom:
            <input
              type="text"
              name="nom"
              value={client.nom}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div>
          <label>
            Prénom:
            <input
              type="text"
              name="prenom"
              value={client.prenom}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div>
          <label>
            Adresse:
            <input
              type="text"
              name="adresse"
              value={client.adresse}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <button type="submit">{id ? 'Modifier' : 'Ajouter'}</button>
      </form>
    </div>
  );
}

export default ClientForm;